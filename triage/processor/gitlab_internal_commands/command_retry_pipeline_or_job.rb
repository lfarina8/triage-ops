# frozen_string_literal: true

require_relative '../../triage/event'
require_relative '../../triage/processor'

module Triage
  class CommandRetryPipelineOrJob < Processor
    include RateLimit

    PIPELINE = 'pipeline'
    JOB = 'job'
    ALLOWED_RESOURCE_TYPES = [PIPELINE, JOB].freeze
    ARGS_REGEX = /\d+$/
    NOT_FOUND_ERROR = 'Gitlab::Error::NotFound'
    # If a job is already retried, the next retry will result in a forbidden error.
    FORBIDDEN_ERROR = 'Gitlab::Error::Forbidden'

    ALLOWED_PROJECTS = [
      Triage::Event::MASTER_BROKEN_INCIDENT_PROJECT_ID,
      Triage::Event::OMNIBUS_PROJECT_ID
    ].freeze

    GITLAB_ORG_DISTRIBUTION_GROUP_ID = 2639717

    react_to 'issue.note'
    define_command name: %w[retry_pipeline retry_job], args_regex: ARGS_REGEX

    def applicable?
      from_allowed_group_or_project? &&
        allowed_user? &&
        project_id_from_incident_label.present? &&
        valid_command_and_args?
    end

    def process
      retry_response = retry_pipeline_or_job

      return unless retry_response

      comment =
        if retry_response.has_key?('web_url')
          <<~MARKDOWN.chomp
          Retried #{retry_resource_type} at #{retry_response.fetch('web_url')}.
          MARKDOWN
        elsif retry_response.has_key?(:error_type)
          error_type = retry_response.fetch(:error_type)
          case error_type
          when NOT_FOUND_ERROR
            "Command failed, #{args_id} is not a valid #{retry_resource_type} id."
          when FORBIDDEN_ERROR
            "Command failed, #{retry_resource_type} #{args_id} cannot be retried at this time. See [troubleshooting guidelines](https://handbook.gitlab.com/handbook/engineering/workflow/#pro-tips-for-triage-dri) for details."
          else
            'Command failed! Try again later.'
          end
        end

      append_discussion(comment, command_discussions_id, append_source_link: false)
    end

    def documentation
      <<~TEXT
        This processor supports the "@gitlab-bot ${retry_pipeline|retry_job} $id" triage operation command to retry a failed pipeline.
      TEXT
    end

    private

    def from_allowed_group_or_project?
      event.from_gitlab_org_distribution? ||
        event.from_gitlab_org_build? ||
        event.from_gitlab_org_cloud_native? ||
        event.from_gitlab_org_charts? ||
        ALLOWED_PROJECTS.include?(event.project_id)
    end

    def allowed_user?
      return event.by_team_member? if event.from_master_broken_incidents_project?

      # Distribution team
      distribution_team_members = Triage.api_client.group_members(Triage::GITLAB_ORG_DISTRIBUTION_GROUP)
      distribution_team_members.map(&:id).include?(event.event_actor_id)
    end

    def project_id_from_incident_label
      return event.project_id unless event.from_master_broken_incidents_project?

      return Triage::Event::GITLAB_PROJECT_ID if event.label_names.include?(Labels::MASTER_BROKEN_LABEL)
      return Triage::Event::GITLAB_FOSS_PROJECT_ID if event.label_names.include?(Labels::MASTER_FOSS_BROKEN_LABEL)
    end

    def valid_command_and_args?
      command.valid?(event) &&
        ALLOWED_RESOURCE_TYPES.include?(retry_resource_type) &&
        command.args(event).size == 1
    end

    def retry_resource_type
      @retry_resource_type ||=
        case command.command(event)
        when 'retry_pipeline'
          PIPELINE
        when 'retry_job'
          JOB
        end
    end

    def client_request
      @client_request ||=
        case command.command(event)
        when 'retry_pipeline'
          :retry_pipeline
        when 'retry_job'
          :job_retry
        end
    end

    def args_id
      command.args(event)[0]
    end

    def command_discussions_id
      event.payload.dig('object_attributes', 'discussion_id')
    end

    def retry_pipeline_or_job
      return unless project_id_from_incident_label

      Triage.api_client.send(client_request, project_id_from_incident_label, args_id)
    rescue Gitlab::Error::NotFound
      { error_type: NOT_FOUND_ERROR }
    rescue Gitlab::Error::Forbidden
      { error_type: FORBIDDEN_ERROR }
    rescue StandardError
      { error_type: 'other' }
    end

    def cache_key
      @cache_key ||= OpenSSL::Digest.hexdigest('SHA256', "retry_pipeline-command-sent-#{event.event_actor_id}-#{event.noteable_path}")
    end

    def rate_limit_count
      100
    end

    def rate_limit_period
      3600 # 1 hour
    end
  end
end
