variables:
  KUBE_CONTEXT: gitlab-org/quality/triage-ops:triage-ops-prod
  KUBE_NAMESPACE: triage-ops-7776928-production
  DEPLOY_IMAGE: "registry.gitlab.com/gitlab-org/gitlab-build-images/debian-bookworm-ruby-3.0:gcloud-383-kubectl-1.26-helm-3.9"

.kube-context:
  before_script:
    - kubectl config use-context ${KUBE_CONTEXT}

.triage-web-patterns: &triage-web-patterns
  - "config/**/*"
  - "triage/**/*"
  - "lib/**/*"
  - "spec/{fixtures/reactive,job,processor,rack,support,triage}/**/*"
  - ".gitlab/ci/triage-web.yml"
  - "Dockerfile.rack"

.ci-patterns: &ci-patterns
  - ".gitlab-ci.yml"
  - ".gitlab/ci/triage-web.yml"

.rules-deploy:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes: *triage-web-patterns
    - when: never # Remove this in a merge request if deploy is needed there
    - when: manual

.rules-general:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - changes: *triage-web-patterns
    - changes: *ci-patterns

build:triage-web:
  stage: build
  extends: .rules-general
  image:
    name: gcr.io/kaniko-project/executor:v1.7.0-debug
    entrypoint: [""]
  before_script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
  script:
    - /kaniko/executor --context=${CI_PROJECT_DIR} --dockerfile=${CI_PROJECT_DIR}/Dockerfile.rack --destination=${TRIAGE_WEB_IMAGE_PATH} --cache=true

deploy:secrets:
  stage: secrets
  environment:
    name: production
    action: prepare
  extends: [".kube-context", ".rules-deploy"]
  needs: []
  image: "${DEPLOY_IMAGE}"
  script:
    - kubectl create secret generic gitlab-webhook-token
        --from-literal GITLAB_WEBHOOK_TOKEN="$GITLAB_WEBHOOK_TOKEN"
        --dry-run=client -o yaml | kubectl apply -n "${KUBE_NAMESPACE}" -f -
    - kubectl create secret generic gitlab-com-api-token
        --from-literal GITLAB_COM_API_TOKEN="$GITLAB_COM_API_TOKEN"
        --dry-run=client -o yaml | kubectl apply -n "${KUBE_NAMESPACE}" -f -
    - kubectl create secret generic gitlab-dev-api-token
        --from-literal GITLAB_DEV_API_TOKEN="$GITLAB_DEV_API_TOKEN"
        --dry-run=client -o yaml | kubectl apply -n "${KUBE_NAMESPACE}" -f -
    - kubectl create secret generic gitlab-dashboard-token
        --from-literal GITLAB_DASHBOARD_TOKEN="$GITLAB_DASHBOARD_TOKEN"
        --dry-run=client -o yaml | kubectl apply -n "${KUBE_NAMESPACE}" -f -
    - kubectl create secret generic sentry-dsn
        --from-literal SENTRY_DSN="$SENTRY_DSN_PRODUCTION"
        --dry-run=client -o yaml | kubectl apply -n "${KUBE_NAMESPACE}" -f -
    - kubectl create secret generic slack-webhook-url
        --from-literal SLACK_WEBHOOK_URL="$CI_SLACK_WEBHOOK_URL"
        --dry-run=client -o yaml | kubectl apply -n "${KUBE_NAMESPACE}" -f -
    - kubectl create secret generic user-account-csv-url
        --from-literal USER_ACCOUNT_CSV_URL="$USER_ACCOUNT_CSV_URL"
        --dry-run=client -o yaml | kubectl apply -n "${KUBE_NAMESPACE}" -f -
    - kubectl create secret generic leading-organizations-csv-url
        --from-literal LEADING_ORGANIZATIONS_CSV_URL="$LEADING_ORGANIZATIONS_CSV_URL"
        --dry-run=client -o yaml | kubectl apply -n "${KUBE_NAMESPACE}" -f -
    - kubectl create secret generic alt-project-id-for-ci-title-label
        --from-literal ALT_PROJECT_ID_FOR_CI_TITLE_LABEL="$ALT_PROJECT_ID_FOR_CI_TITLE_LABEL"
        --dry-run=client -o yaml | kubectl apply -n "${KUBE_NAMESPACE}" -f -
    - kubectl create secret generic discord-webhook-path-community-mr-request-review
        --from-literal DISCORD_WEBHOOK_PATH_COMMUNITY_MR_REQUEST_REVIEW="$DISCORD_WEBHOOK_PATH_COMMUNITY_MR_REQUEST_REVIEW"
        --dry-run=client -o yaml | kubectl apply -n "${KUBE_NAMESPACE}" -f -
    - kubectl create secret generic discord-webhook-path-community-mr-help
        --from-literal DISCORD_WEBHOOK_PATH_COMMUNITY_MR_HELP="$DISCORD_WEBHOOK_PATH_COMMUNITY_MR_HELP"
        --dry-run=client -o yaml | kubectl apply -n "${KUBE_NAMESPACE}" -f -
    - kubectl create secret generic discord-webhook-path-community-mr-handoff
        --from-literal DISCORD_WEBHOOK_PATH_COMMUNITY_MR_HANDOFF="$DISCORD_WEBHOOK_PATH_COMMUNITY_MR_HANDOFF"
        --dry-run=client -o yaml | kubectl apply -n "${KUBE_NAMESPACE}" -f -
    - kubectl create secret generic common-room-api-token
        --from-literal COMMON_ROOM_API_TOKEN="$COMMON_ROOM_API_TOKEN"
        --dry-run=client -o yaml | kubectl apply -n "${KUBE_NAMESPACE}" -f -

deploy:triage-web:
  stage: deploy
  environment:
    name: production
    action: start
  extends: [".kube-context", ".rules-deploy"]
  needs: ["build:triage-web"]
  image: "${DEPLOY_IMAGE}"
  script:
    - echo ${KUBE_NAMESPACE}
    - kubectl apply -f config/triage-web.yaml -n ${KUBE_NAMESPACE}
    - kubectl set image deployment/triage-web-deployment triage-web=${TRIAGE_WEB_IMAGE_PATH} -n ${KUBE_NAMESPACE}
    - kubectl rollout restart deployment/triage-web-deployment -n ${KUBE_NAMESPACE}
    - kubectl get all -n ${KUBE_NAMESPACE}

health-check:container:
  stage: health-check
  extends:
    - .rules-general
    - .use-docker-in-docker
  variables:
    CONTAINER_NAME: triage-web
  before_script:
    - export SERVICE_URL=http://${CONTAINER_NAME}:8080
    - docker network create test
    - docker run -d -p 8080:8080 --name ${CONTAINER_NAME} --net test --hostname ${CONTAINER_NAME} -e GITLAB_WEBHOOK_TOKEN=gitlab_webhook_token -e GITLAB_COM_API_TOKEN=gitlab_api_token -e SLACK_WEBHOOK_URL='' -e DRY_RUN=1 ${TRIAGE_WEB_IMAGE_PATH}
    - docker ps
    - docker logs -f ${CONTAINER_NAME} &> container.log &
  script:
    - docker run --rm --net test -v "$PWD:/work" curlimages/curl /work/bin/health-check $SERVICE_URL /work gitlab_webhook_token
    - docker stop ${CONTAINER_NAME}
    - docker network rm test
    - test ! -f health-check-failed
  artifacts:
    paths:
      - container.log
    expire_in: 31d
    when: always
