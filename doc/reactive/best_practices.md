# Best Practices

[[_TOC_]]

## Use the sandbox to test new processors

We can use `event.from_gitlab_org?(sandbox: true)` or `event.from_gitlab_com?(sandbox: true)` to make a new processor only process events coming from [the triage-ops sandbox project](https://gitlab.com/gitlab-org/quality/engineering-productivity/triage-ops-playground).

You can enable the sandbox for a new processor by editing the `applicable?` method as follows:

```ruby
# Before
def applicable?
  event.from_gitlab_org?
end

# After
def applicable?
  event.from_gitlab_org?(sandbox: true)
end
```

This example above will make your processor react to events coming from the triage sandbox project, instead of events coming from the `gitlab-org` group.

Once your processor has been proven to work, you can remove the `sandbox: true` in another MR:

```ruby
# Before
def applicable?
  event.from_gitlab_org?(sandbox: true)
end

# After
def applicable?
  event.from_gitlab_org?
end
```
